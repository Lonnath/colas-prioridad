/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author Lonnath
 */
public class TrabajoImpresion implements Comparable<TrabajoImpresion> {
    private int paper;
    private int time;
    public TrabajoImpresion(int p, int t){
        this.paper = p;
        this.time = t;
    }
    public int getPaper(){
        return this.paper;
    }
    public int getTime(){
        return this.time;
    }
    public String toString(){
        return "Cantidad De Hojas: "+this.paper+" - Tiempo de Creación: "+this.time;
    }

    @Override
    public int compareTo(TrabajoImpresion o) {
          if(this.paper>o.getPaper()){
            return 1;
        }else if(this.paper<o.getPaper()){
            return -1;
        }else{
            if(this.time>o.getTime()){
                return 1;
            }else if(this.time<o.getTime()){
                return-1;
            }
            
            return 0;
        }}
    
}
