/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;
import java.util.Scanner;
import Model.SistemaImpresion;
/**
 *
 * @author Lonnath
 */
public class Test {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Cantidad de Trabajos Por Imprimir");
        SistemaImpresion system = new SistemaImpresion();
        int a = sc.nextInt();
        for (int i = 1; i <= a; i++) {
            System.out.println(i+". Digite La Cantidad de Hojas Del Trabajo ");
            int p = sc.nextInt();
            System.out.println("Digite Tiempo De Creación");
            int t = sc.nextInt();
            system.insertWork(p, t);
        }
        System.out.println("Cola De Prioridad: "+ system.toString());
    }
}
