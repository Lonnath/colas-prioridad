/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import Entity.TrabajoImpresion;
import java.util.Queue;
import java.util.PriorityQueue;
/**
 *
 * @author Lonnath
 */
public class SistemaImpresion {
    private Queue <TrabajoImpresion> work;

    public SistemaImpresion(){
        this.work = new PriorityQueue();
    }
    public void insertWork(int p, int t){
        work.add(new TrabajoImpresion(p,t));
    }
    public String toString(){
        String output = "";
        int tmp = 1;
        Queue<TrabajoImpresion> tmpW = new PriorityQueue();
        while(!this.work.isEmpty()){
            TrabajoImpresion tmpT = this.work.poll();
            tmpW.add(tmpT);
            output+="\n"+tmp+". "+tmpT.toString()+".";
            tmp++;
        }
        while(!tmpW.isEmpty()){
            this.work.add(tmpW.poll());
        }
        return output;
    }
    
}
